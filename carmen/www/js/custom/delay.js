var deps = [];

angular
  .module('delay', deps);

angular
  .module('delay')
  .directive('delay', ['$document', '$parse',
    function ($document, $parse) {
      return {
        link: function (scope, iElement, iAttrs) {
          var delay = iAttrs.delay;
          delay += 's ';
          console.log(delay);
          iElement.css({
            'animation-delay': delay,
            '-webkit-animation-delay': delay,
            '-moz-animation-delay': delay
          })
        }
      };
    }])
