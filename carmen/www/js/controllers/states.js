var deps = [];

angular
  .module('states', deps);

angular
  .module('states')
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('carmen', {
        url: '/carmen',
        abstract: true,
        templateUrl: 'views/main.html'
      })
      .state('carmen.splash', {
        url: '/splash',
        views: {
          'carmen-splash': {
            templateUrl: 'views/splash.html',
          }
        }
      })
      .state('carmen.list', {
        url: '/list',
        views: {
          'carmen-list': {
            templateUrl: 'views/artists_list.html',
            controller: 'artists_controller'
          }
        }
      })
      .state('carmen.details', {
        url: '/list/:oId',
        views: {
          'carmen-details': {
            templateUrl: 'views/artist_details.html',
            controller: 'artist_detail'
          }
        }
      });
    $urlRouterProvider.otherwise('/carmen/splash');
  });
