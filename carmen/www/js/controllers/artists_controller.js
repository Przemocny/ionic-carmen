var deps = [];

angular
  .module('artists', deps);

angular
  .module('artists')
  .controller('artists_controller', ['$scope', 'Artists', function ($scope, Artists) {
    $scope.artists = Artists.all();
  }])
  .controller('artist_detail', ['$scope', 'Artists','$stateParams', function ($scope, Artists, $stateParams) {
    $scope.artist_detail = Artists.get($stateParams.oId);
  }])



