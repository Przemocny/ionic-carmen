angular
  .module('artists')
  .factory('Artists', function () {
    var realElem = {
      name: 'Carmen',
      author: 'Georges Bizet',
      author_lives: '1838-1875',
      thumb: 'img/opery_btn_01.jpg',
      splash: 'img/carmen_background.jpg',
      desc: 'Ciężka sprawa, bo ten tim nie daje sobie rady z tak prostymi czelendżami, dlatego leć po mleko do kawy. Jest nowy Pan Kanapka o 11:30. Tak jak Adrian pisał w mailu - na premie w tym roku nie ma co liczyć. Na szczęście to będzie masakrycznie trudny czelendż, także trzeba się dobrze przygotować, maks. 3 minuty. Zresztą chyba czytałeś w njusleterze: szykują się zwolnienia.'
    };
    realElem.author_slug = dummyData().slugify(realElem.author);
    var obj_list = [realElem];
    for (var i = 0; i < 19; i++) {
      var tempObj = {
        name: dummyData().fancyName(),
        author: dummyData().fullName(),
        author_lives: dummyData().timeRange(),
        thumb: dummyData().photo('500x300'),
        splash: dummyData().photo('1200x2000'),
        desc: dummyData().words(40)
      }
      tempObj.author_slug = dummyData().slugify(tempObj.author);
      obj_list.push(tempObj);
    }

    return {
      all: function () {
        return obj_list;
      },
      get: function (id) {
        return obj_list[id];
      }
    }
  });

//.controller('artists_controller', ['$scope','$state', function ($scope, $state) {
//  var realElem = {
//    name: 'Carmen',
//    author: 'Georges Bizet',
//    author_lives: '1838-1875',
//    thumb: 'img/opery_btn_01.jpg',
//    splash: 'img/carmen_background.jpg',
//    desc: 'Ciężka sprawa, bo ten tim nie daje sobie rady z tak prostymi czelendżami, dlatego leć po mleko do kawy. Jest nowy Pan Kanapka o 11:30. Tak jak Adrian pisał w mailu - na premie w tym roku nie ma co liczyć. Na szczęście to będzie masakrycznie trudny czelendż, także trzeba się dobrze przygotować, maks. 3 minuty. Zresztą chyba czytałeś w njusleterze: szykują się zwolnienia.'
//  };
//  realElem.author_slug = dummyData().slugify(realElem.author);
//  var obj_list = [realElem];
//  for (var i = 0; i < 20 - 1; i++) {
//    var tempObj = {
//      name: dummyData().fancyName(),
//      author: dummyData().fullName(),
//      author_lives: dummyData().timeRange(),
//      thumb: dummyData().photo('500x300'),
//      splash: dummyData().photo('1200x2000'),
//      desc: dummyData().words(40)
//    }
//    tempObj.author_slug = dummyData().slugify(tempObj.author);
//    obj_list.push(tempObj);
//  }
//  $scope.artists = obj_list;
//
//  $scope.routeSelector = $state.params.operaID;
//}]);

function dummyData() {
  var data = {};
  // basics
  data.number_int = function (minimum, maximum) {
    var min = minimum || 0;
    var max = maximum || 10;
    var random = Math.floor(Math.random() * max) + min;
    return random
  }
  data.words = function (length) {
    var words = [
      'Vivamus', 'purus', 'orci', 'tincidunt', 'tincidunt', 'sollicitudin', 'eu', 'convallis',
      'efficitur', 'urna', 'Donec', 'nec', 'sollicitudin', 'felis', 'Pellentesque', 'habitant',
      'morbi', 'tristique', 'senectus', 'et', 'netus', 'et', 'malesuada', 'fames', 'ac', 'turpis', '' +
      'egestas', '. Proin', 'ex', 'felis', 'viverra', 'in', 'erat', 'id', 'condimentum', 'elementum',
      'felis', '. Etiam', 'ccumsan', 'est', 'in', 'posuere', 'feugiat', 'odio', 'nibh', 'luctus',
      'urna', 'in', 'fermentum', 'justo', 'metus',
      '. Vivamus', 'purus', 'orci', 'tincidunt', 'tincidunt', 'sollicitudin', 'eu', 'convallis',
      'efficitur', 'urna', 'Donec', 'nec', 'sollicitudin', 'felis', 'Pellentesque', 'habitant',
      'morbi', 'tristique', 'senectus', 'et', 'netus', 'et', 'malesuada', 'fames', 'ac', 'turpis', '' +
      'egestas', '. Proin', 'ex', 'felis', 'viverra', 'in', 'erat', 'id', 'condimentum', 'elementum',
      'felis', 'Etiam', 'ccumsan', 'est', 'in', 'posuere', 'feugiat', 'odio', 'nibh', 'luctus',
      'urna', 'in', 'fermentum', 'justo', 'metus',
      'Vivamus', 'purus', 'orci', 'tincidunt', 'tincidunt', 'sollicitudin', 'eu', 'convallis',
      'efficitur', 'urna', 'Donec', 'nec', 'sollicitudin', 'felis', 'Pellentesque', 'habitant',
      'morbi', 'tristique', 'senectus', 'et', 'netus', 'et', 'malesuada', 'fames', 'ac', 'turpis', '' +
      'egestas', '. Proin', 'ex', 'felis', 'viverra', 'in', 'erat', 'id', 'condimentum', 'elementum',
      'felis', '. Etiam', 'ccumsan', 'est', 'in', 'posuere', 'feugiat', 'odio', 'nibh', 'luctus',
      'urna', 'in', 'fermentum', 'justo', 'metus',
      '. Vivamus', 'purus', 'orci', 'tincidunt', 'tincidunt', 'sollicitudin', 'eu', 'convallis',
      'efficitur', 'urna', 'Donec', 'nec', 'sollicitudin', 'felis', 'Pellentesque', 'habitant',
      'morbi', 'tristique', 'senectus', 'et', 'netus', 'et', 'malesuada', 'fames', 'ac', 'turpis', '' +
      'egestas', '. Proin', 'ex', 'felis', 'viverra', 'in', 'erat', 'id', 'condimentum', 'elementum',
      'felis', 'Etiam', 'ccumsan', 'est', 'in', 'posuere', 'feugiat', 'odio', 'nibh', 'luctus',
      'urna', 'in', 'fermentum', 'justo', 'metus',
      'Vivamus', 'purus', 'orci', 'tincidunt', 'tincidunt', 'sollicitudin', 'eu', 'convallis',
      'efficitur', 'urna', 'Donec', 'nec', 'sollicitudin', 'felis', 'Pellentesque', 'habitant',
      'morbi', 'tristique', 'senectus', 'et', 'netus', 'et', 'malesuada', 'fames', 'ac', 'turpis', '' +
      'egestas', '. Proin', 'ex', 'felis', 'viverra', 'in', 'erat', 'id', 'condimentum', 'elementum',
      'felis', '. Etiam', 'ccumsan', 'est', 'in', 'posuere', 'feugiat', 'odio', 'nibh', 'luctus',
      'urna', 'in', 'fermentum', 'justo', 'metus',
      '. Vivamus', 'purus', 'orci', 'tincidunt', 'tincidunt', 'sollicitudin', 'eu', 'convallis',
      'efficitur', 'urna', 'Donec', 'nec', 'sollicitudin', 'felis', 'Pellentesque', 'habitant',
      'morbi', 'tristique', 'senectus', 'et', 'netus', 'et', 'malesuada', 'fames', 'ac', 'turpis', '' +
      'egestas', '. Proin', 'ex', 'felis', 'viverra', 'in', 'erat', 'id', 'condimentum', 'elementum',
      'felis', 'Etiam', 'ccumsan', 'est', 'in', 'posuere', 'feugiat', 'odio', 'nibh', 'luctus',
      'urna', 'in', 'fermentum', 'justo', 'metus',

    ]
    var newString = '';
    var len = length || 10;
    var random = Math.floor(Math.random() * len) + 3;
    for (var i = 0; i < random; i++) {
      var rnd = Math.floor(Math.random() * words.length);
      newString += words[rnd] + ' ';
    }
    return newString
  }
  data.firstName = function () {
    var arr = [
      {
        "who": "Ollie"
      },
      {
        "who": "Becker"
      },
      {
        "who": "Blackburn"
      },
      {
        "who": "Marion"
      },
      {
        "who": "Mathis"
      },
      {
        "who": "Janelle"
      },
      {
        "who": "Cecilia"
      },
      {
        "who": "Meyers"
      },
      {
        "who": "Marisol"
      },
      {
        "who": "Lee"
      },
      {
        "who": "Raquel"
      },
      {
        "who": "Kasey"
      },
      {
        "who": "Belinda"
      },
      {
        "who": "Katheryn"
      },
      {
        "who": "Blanchard"
      },
      {
        "who": "Conway"
      },
      {
        "who": "Harding"
      },
      {
        "who": "Ginger"
      },
      {
        "who": "Key"
      },
      {
        "who": "Dorthy"
      },
      {
        "who": "Lindsey"
      },
      {
        "who": "Dorothea"
      },
      {
        "who": "Bianca"
      },
      {
        "who": "Erickson"
      },
      {
        "who": "Stephenson"
      },
      {
        "who": "Stokes"
      },
      {
        "who": "Latasha"
      },
      {
        "who": "Francis"
      },
      {
        "who": "Bass"
      },
      {
        "who": "Ford"
      },
      {
        "who": "Patrick"
      },
      {
        "who": "Elba"
      },
      {
        "who": "Darlene"
      },
      {
        "who": "Judith"
      },
      {
        "who": "Murphy"
      },
      {
        "who": "Leonard"
      },
      {
        "who": "Milagros"
      },
      {
        "who": "Beulah"
      },
      {
        "who": "Grant"
      },
      {
        "who": "Lorena"
      },
      {
        "who": "Jackie"
      },
      {
        "who": "Little"
      },
      {
        "who": "Gilmore"
      },
      {
        "who": "Wilkinson"
      },
      {
        "who": "Bush"
      },
      {
        "who": "Morales"
      },
      {
        "who": "Jane"
      },
      {
        "who": "Latonya"
      },
      {
        "who": "Jacklyn"
      },
      {
        "who": "Lindsay"
      },
      {
        "who": "Myrtle"
      },
      {
        "who": "Kennedy"
      },
      {
        "who": "Loraine"
      },
      {
        "who": "Amalia"
      },
      {
        "who": "Stark"
      },
      {
        "who": "Armstrong"
      },
      {
        "who": "Hunt"
      },
      {
        "who": "Baxter"
      },
      {
        "who": "Graves"
      },
      {
        "who": "Greene"
      },
      {
        "who": "Morrow"
      },
      {
        "who": "Summers"
      },
      {
        "who": "Jackson"
      },
      {
        "who": "Mason"
      },
      {
        "who": "Roxie"
      },
      {
        "who": "Elliott"
      },
      {
        "who": "Gamble"
      },
      {
        "who": "Benjamin"
      },
      {
        "who": "Kari"
      },
      {
        "who": "Hernandez"
      },
      {
        "who": "Osborn"
      },
      {
        "who": "Barry"
      },
      {
        "who": "Powers"
      },
      {
        "who": "Terra"
      },
      {
        "who": "Cantu"
      },
      {
        "who": "Cohen"
      },
      {
        "who": "Kay"
      },
      {
        "who": "Sykes"
      },
      {
        "who": "Matilda"
      },
      {
        "who": "Lester"
      },
      {
        "who": "Lambert"
      },
      {
        "who": "Lelia"
      },
      {
        "who": "Barrett"
      },
      {
        "who": "Florine"
      },
      {
        "who": "Fernandez"
      },
      {
        "who": "Cornelia"
      },
      {
        "who": "Hughes"
      },
      {
        "who": "Maynard"
      },
      {
        "who": "Clarke"
      },
      {
        "who": "Pugh"
      },
      {
        "who": "Lisa"
      },
      {
        "who": "Munoz"
      },
      {
        "who": "Kerr"
      },
      {
        "who": "Cochran"
      },
      {
        "who": "Freda"
      },
      {
        "who": "Teri"
      },
      {
        "who": "Latisha"
      },
      {
        "who": "Ramsey"
      },
      {
        "who": "Bentley"
      },
      {
        "who": "Katrina"
      }
    ];
    var random = Math.floor(Math.random() * arr.length);
    return arr[random].who;
  };
  data.lastName = function () {
    var arr = [
      {
        "who": "Houston"
      },
      {
        "who": "Daugherty"
      },
      {
        "who": "Poole"
      },
      {
        "who": "Patel"
      },
      {
        "who": "Everett"
      },
      {
        "who": "Moses"
      },
      {
        "who": "Marks"
      },
      {
        "who": "Hogan"
      },
      {
        "who": "George"
      },
      {
        "who": "Winters"
      },
      {
        "who": "Ochoa"
      },
      {
        "who": "Delaney"
      },
      {
        "who": "Wolfe"
      },
      {
        "who": "Vincent"
      },
      {
        "who": "Briggs"
      },
      {
        "who": "Woodard"
      },
      {
        "who": "Rivera"
      },
      {
        "who": "Stanley"
      },
      {
        "who": "Blackwell"
      },
      {
        "who": "Blankenship"
      },
      {
        "who": "Burt"
      },
      {
        "who": "Chambers"
      },
      {
        "who": "Bowen"
      },
      {
        "who": "Suarez"
      },
      {
        "who": "Tyler"
      },
      {
        "who": "Morales"
      },
      {
        "who": "Ratliff"
      },
      {
        "who": "Reed"
      },
      {
        "who": "Grant"
      },
      {
        "who": "Taylor"
      },
      {
        "who": "Carver"
      },
      {
        "who": "Miller"
      },
      {
        "who": "Park"
      },
      {
        "who": "Battle"
      },
      {
        "who": "Ayala"
      },
      {
        "who": "Mcgowan"
      },
      {
        "who": "Glenn"
      },
      {
        "who": "Long"
      },
      {
        "who": "Hobbs"
      },
      {
        "who": "Farley"
      },
      {
        "who": "Munoz"
      },
      {
        "who": "Slater"
      },
      {
        "who": "Hurley"
      },
      {
        "who": "Mccullough"
      },
      {
        "who": "Mayer"
      },
      {
        "who": "Moss"
      },
      {
        "who": "Frye"
      },
      {
        "who": "Nash"
      },
      {
        "who": "Conrad"
      },
      {
        "who": "Ford"
      },
      {
        "who": "Floyd"
      },
      {
        "who": "Casey"
      },
      {
        "who": "Terry"
      },
      {
        "who": "Roman"
      },
      {
        "who": "Juarez"
      },
      {
        "who": "Roberts"
      },
      {
        "who": "Vang"
      },
      {
        "who": "Bryant"
      },
      {
        "who": "Medina"
      },
      {
        "who": "Cooper"
      },
      {
        "who": "Hopkins"
      },
      {
        "who": "Dudley"
      },
      {
        "who": "Bentley"
      },
      {
        "who": "Obrien"
      },
      {
        "who": "Frazier"
      },
      {
        "who": "Underwood"
      },
      {
        "who": "Dotson"
      },
      {
        "who": "Williams"
      },
      {
        "who": "Fleming"
      },
      {
        "who": "Mueller"
      },
      {
        "who": "Townsend"
      },
      {
        "who": "Lindsey"
      },
      {
        "who": "Myers"
      },
      {
        "who": "Rush"
      },
      {
        "who": "Francis"
      },
      {
        "who": "Molina"
      },
      {
        "who": "Cruz"
      },
      {
        "who": "Bauer"
      },
      {
        "who": "Bartlett"
      },
      {
        "who": "Fischer"
      },
      {
        "who": "Moran"
      },
      {
        "who": "Montgomery"
      },
      {
        "who": "Santiago"
      },
      {
        "who": "Kelly"
      },
      {
        "who": "Vaughn"
      },
      {
        "who": "Hooper"
      },
      {
        "who": "Oneal"
      },
      {
        "who": "Turner"
      },
      {
        "who": "Garcia"
      },
      {
        "who": "Mcmillan"
      },
      {
        "who": "Levine"
      },
      {
        "who": "Moon"
      },
      {
        "who": "Dillard"
      },
      {
        "who": "Ortiz"
      },
      {
        "who": "Hood"
      },
      {
        "who": "Goodwin"
      },
      {
        "who": "Mullen"
      },
      {
        "who": "Gilbert"
      },
      {
        "who": "Oneil"
      },
      {
        "who": "Wheeler"
      }
    ];
    var random = Math.floor(Math.random() * arr.length);
    return arr[random].who;
  };
  data.fullName = function () {
    var s = data.firstName() + ' ' + data.lastName();
    return s;
  };
  data.email = function () {
    var s = data.firstName().toLowerCase() + '.' + data.lastName().toLowerCase() + '@gmail.com';
    return s;
  };
  data.photo = function (wxh, text) {
    var size = wxh || '500x500';
    var t = text || data.fancyName();

    var s = 'http://fakeimg.pl/' + size + '/?text=' + t + '&font=lobster';
    return s;
  };
  data.price = function (ilosc10) {
    var numbers = [
      '0', '1', '2', '3', '4', '5', '6', '7',
      '8', '9'
    ]
    var mnoznik = ilosc10 || 6;
    var price = '';
    for (var i = 0; i < mnoznik - 3; i++) {
      var random = Math.floor(Math.random() * 10);
      if (random == '0') {

      }
      else {
        price += numbers[random];
      }
    }
    price += '000';
    return {netto: Math.floor(price), brutto: Math.floor(price * 1.23)}
  }
  data.numberTel = function () {
    var numbers = [
      '0', '1', '2', '3', '4', '5', '6', '7',
      '8', '9'
    ]
    var tel = '';
    for (var i = 0; i < 9; i++) {
      var random = Math.floor(Math.random() * 10);
      tel += numbers[random];
      if (i == 2 || i == 5 || i == 8) {
        tel += ' ';
      }
    }
    console.log(tel);
    return tel
  }
  data.currency = function (i) {
    var currency = ["PLN", "USD", "EURO"];
    if (i) {
      return currency[i];
    }
    else {
      var word;
      var random = Math.floor(Math.random() * currency.length);
      word = currency[random];
      return word;
    }

  }
  data.status_123 = function () {
    var status = [1, 2, 3];
    var word;
    var random = Math.floor(Math.random() * status.length);
    word = status[random];
    return word;
  }
  data.boolean_number = function () {
    var random = Math.round(Math.random());
    return random;
  }
  data.fancyName = function () {
    var names = [
      {
        "company": "Eventex"
      },
      {
        "company": "Pivitol"
      },
      {
        "company": "Buzzopia"
      },
      {
        "company": "Ginkogene"
      },
      {
        "company": "Daycore"
      },
      {
        "company": "Magmina"
      },
      {
        "company": "Netplax"
      },
      {
        "company": "Olympix"
      },
      {
        "company": "Minga"
      },
      {
        "company": "Orbin"
      },
      {
        "company": "Kiosk"
      },
      {
        "company": "Dogtown"
      },
      {
        "company": "Endipine"
      },
      {
        "company": "Sustenza"
      },
      {
        "company": "Plasmosis"
      },
      {
        "company": "Furnigeer"
      },
      {
        "company": "Tsunamia"
      },
      {
        "company": "Menbrain"
      },
      {
        "company": "Goko"
      },
      {
        "company": "Buzzworks"
      },
      {
        "company": "Endipin"
      },
      {
        "company": "Eyewax"
      },
      {
        "company": "Rodeology"
      },
      {
        "company": "Eplosion"
      },
      {
        "company": "Arctiq"
      },
      {
        "company": "Portaline"
      },
      {
        "company": "Omnigog"
      },
      {
        "company": "Orbean"
      },
      {
        "company": "Ramjob"
      },
      {
        "company": "Edecine"
      },
      {
        "company": "Zounds"
      },
      {
        "company": "Kengen"
      },
      {
        "company": "Kraggle"
      },
      {
        "company": "Geekology"
      },
      {
        "company": "Quilk"
      },
      {
        "company": "Zenthall"
      },
      {
        "company": "Ezentia"
      },
      {
        "company": "Sarasonic"
      },
      {
        "company": "Techade"
      },
      {
        "company": "Comcur"
      },
      {
        "company": "Updat"
      },
      {
        "company": "Assitia"
      },
      {
        "company": "Soprano"
      },
      {
        "company": "Zilla"
      },
      {
        "company": "Isis"
      },
      {
        "company": "Combogen"
      },
      {
        "company": "Volax"
      },
      {
        "company": "Datagen"
      },
      {
        "company": "Eclipto"
      },
      {
        "company": "Exposa"
      },
      {
        "company": "Exovent"
      },
      {
        "company": "Xelegyl"
      },
      {
        "company": "Icology"
      },
      {
        "company": "Entropix"
      },
      {
        "company": "Slumberia"
      },
      {
        "company": "Netur"
      },
      {
        "company": "Caxt"
      },
      {
        "company": "Namegen"
      },
      {
        "company": "Combot"
      },
      {
        "company": "Candecor"
      },
      {
        "company": "Geostele"
      },
      {
        "company": "Telequiet"
      },
      {
        "company": "Perkle"
      },
      {
        "company": "Zilencio"
      },
      {
        "company": "Signity"
      },
      {
        "company": "Tubesys"
      },
      {
        "company": "Gazak"
      },
      {
        "company": "Tersanki"
      },
      {
        "company": "Datacator"
      },
      {
        "company": "Earthpure"
      },
      {
        "company": "Diginetic"
      },
      {
        "company": "Ecolight"
      },
      {
        "company": "Kaggle"
      },
      {
        "company": "Paragonia"
      },
      {
        "company": "Intradisk"
      },
      {
        "company": "Quintity"
      },
      {
        "company": "Nspire"
      },
      {
        "company": "Norali"
      },
      {
        "company": "Lumbrex"
      },
      {
        "company": "Bostonic"
      },
      {
        "company": "Wazzu"
      },
      {
        "company": "Futurity"
      },
      {
        "company": "Norsul"
      },
      {
        "company": "Geofarm"
      },
      {
        "company": "Bizmatic"
      },
      {
        "company": "Maineland"
      },
      {
        "company": "Adornica"
      },
      {
        "company": "Bytrex"
      },
      {
        "company": "Zentility"
      },
      {
        "company": "Farmage"
      },
      {
        "company": "Accusage"
      },
      {
        "company": "Yurture"
      },
      {
        "company": "Turnling"
      },
      {
        "company": "Genmex"
      },
      {
        "company": "Rooforia"
      },
      {
        "company": "Marqet"
      },
      {
        "company": "Ziore"
      },
      {
        "company": "Velity"
      },
      {
        "company": "Vendblend"
      },
      {
        "company": "Pharmex"
      }
    ];
    var word;
    var random = Math.floor(Math.random() * names.length);
    word = names[random].company;
    return word;
  }
  data.sex = function () {
    var sex = [
      'female', 'male', 'uni'
    ];
    var word;
    var random = Math.floor(Math.random() * (sex.length ));
    word = sex[random];
    return word;
  }
  data.category = function () {
    var sex = [
      0, 1, 2, 3
    ];
    var word;
    var random = Math.floor(Math.random() * (sex.length ));
    word = sex[random];
    return word;
  }
  data.time = function () {
    var time = Date.now();
    var random = Math.floor(Math.random() * 6048000000 * 4);
    return {
      close: Math.round(time + random),
      past: Math.round(time - random * 54),
    };
  }
  data.timeRange = function () {
    var time = new Date().getFullYear();
    var random = Math.floor(Math.random() * 200);
    var getStartedYear = Math.round(time - random);
    var getEndedYear = Math.round(time - random / 2);
    var range = getStartedYear.toString() + '-' + getEndedYear.toString();
    return range;
  }

  data.slugify = function (s) {
    var tempStr;
    var polishletters = 'ą ć ę ł ń ó s ź ż'.split(' ');
    var slugletters = 'a c e l n o s z z'.split(' ');
    tempStr = s.toLowerCase().replace(' ', '-');
    for (var i in polishletters) {
      tempStr = tempStr.replace(polishletters[i], slugletters[i]);
    }
    return tempStr;
  }
  // !basics


  return data;

}

